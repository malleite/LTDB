'''
Created on Sep 5, 2014

@author: leite

This code reads and format the ABBA Board data according to Jasmin/Nicolas

-----------------------------------------------------------------------------------------------------------
-- ABBA packet format example for 10 samples : 614 words of 32bits
-----------------------------------------------------------------------------------------------------------
-- HEADER--------------------------------------------------------------------------------------------------
0x0:Header[0]    StartOfHeader    0xEE1234EE ?? for ABBA
0x1:Header[1]    HeaderSize    0x00000009
0x2:Header[2]    FormatVersion    0x02040000
0x3:Header[3]    SourceId    0xABBA0001
0x4:Header[4]    RunNumber    Value to be configured before Run
0x5:Header[5]    L1AId        L1A counter
0x6:Header[6]    BCId        BC counter
0x7:Header[7]    L1ATriggerType    L1A Trigger type
0x8:Header[8]    DetectorEvtType    Value to be configured before Run
0x9:Header[9]    ABBA Header    0xABBACACA
-- DATA----------------------------------------------------------------------------------------------------
-- Header
0xA:Optics Header
-- Sample 0 for Fiber0/1 : sample0 read from actual circular buffer address - L1A latency
0xB :Data  [0]    Fiber0_Sample0_ADC1<11-00> & Fiber0_Sample0_ADC0<11-00> & Fiber1_Sample0_ADC1<11-04>
0xC :Data  [1]    Fiber1_Sample0_ADC1<03-00> & Fiber1_Sample0_ADC0<11-00> & Fiber0_Sample0_ADC3<11-00> & Fiber0_Sample0_ADC2<11-08>
0xD :Data  [2]    Fiber0_Sample0_ADC2<07-00> & Fiber1_Sample0_ADC3<11-00> & Fiber1_Sample0_ADC2<11-00>
0xE :Data  [3]    Fiber0_Sample0_ADC5<11-00> & Fiber0_Sample0_ADC4<11-00> & Fiber1_Sample0_ADC5<11-04>
0xF :Data  [4]    Fiber1_Sample0_ADC5<03-00> & Fiber1_Sample0_ADC4<11-00> & Fiber0_Sample0_ADC7<11-00> & Fiber0_Sample0_ADC6<11-08>
0x10:Data  [5]    Fiber0_Sample0_ADC6<07-00> & Fiber1_Sample0_ADC7<11-00> & Fiber1_Sample0_ADC6<11-00>
-- Sample 1 for Fiber0/1 : sample1 read from actual circular buffer address - L1A latency + 1x25ns
0x10:Data [6]    Fiber0_Sample1_ADC1<11-00> & Fiber0_Sample1_ADC0<11-00> & Fiber1_Sample1_ADC1<11-04>
0x11:Data [7]    Fiber1_Sample1_ADC1<03-00> & Fiber1_Sample1_ADC0<11-00> & Fiber0_Sample1_ADC3<11-00> & Fiber0_Sample0_ADC2<11-08>
0x12:Data [8]    Fiber0_Sample1_ADC2<07-00> & Fiber1_Sample1_ADC3<11-00> & Fiber1_Sample1_ADC2<11-00>
0x13:Data [9]    Fiber0_Sample1_ADC5<11-00> & Fiber0_Sample1_ADC4<11-00> & Fiber1_Sample1_ADC5<11-04>
0x14:Data [10]    Fiber1_Sample1_ADC5<03-00> & Fiber1_Sample1_ADC4<11-00> & Fiber0_Sample1_ADC7<11-00> & Fiber0_Sample0_ADC6<11-08>
0x15:Data [11]    Fiber0_Sample1_ADC6<07-00> & Fiber1_Sample1_ADC7<11-00> & Fiber1_Sample1_ADC6<11-00>
.
.
.
-- Sample 9 for Fiber0/1 : sample9 read from actual circular buffer address - L1A latency + 9x25ns
0x4B:Data [54]    Fiber0_Sample9_ADC1<11-00> & Fiber0_Sample9_ADC0<11-00> & Fiber1_Sample9_ADC1<11-04>
0x4C:Data [55]    Fiber1_Sample9_ADC1<03-00> & Fiber1_Sample9_ADC0<11-00> & Fiber0_Sample9_ADC3<11-00> & Fiber0_Sample0_ADC2<11-08>
0x4D:Data [56]    Fiber0_Sample9_ADC2<07-00> & Fiber1_Sample9_ADC3<11-00> & Fiber1_Sample9_ADC2<11-00>
0x4E:Data [57]    Fiber0_Sample9_ADC5<11-00> & Fiber0_Sample9_ADC4<11-00> & Fiber1_Sample9_ADC5<11-04>
0x4F:Data [58]    Fiber1_Sample9_ADC5<03-00> & Fiber1_Sample9_ADC4<11-00> & Fiber0_Sample9_ADC7<11-00> & Fiber0_Sample0_ADC6<11-08>
0x50:Data [59]    Fiber0_Sample9_ADC6<07-00> & Fiber1_Sample9_ADC7<11-00> & Fiber1_Sample9_ADC6<11-00>
-- Sample 0 for Fiber2/3 : sample0 read from actual circular buffer address - L1A latency
0x51:Data [60]    Fiber2_Sample0_ADC1<11-00> & Fiber2_Sample0_ADC0<11-00> & Fiber3_Sample0_ADC1<11-04>
0x52:Data [61]    Fiber3_Sample0_ADC1<03-00> & Fiber3_Sample0_ADC0<11-00> & Fiber2_Sample0_ADC3<11-00> & Fiber0_Sample0_ADC2<11-08>
0x53:Data [62]    Fiber2_Sample0_ADC2<07-00> & Fiber3_Sample0_ADC3<11-00> & Fiber3_Sample0_ADC2<11-00>
0x54:Data [63]    Fiber2_Sample0_ADC5<11-00> & Fiber2_Sample0_ADC4<11-00> & Fiber3_Sample0_ADC5<11-04>
0x55:Data [64]    Fiber3_Sample0_ADC5<03-00> & Fiber3_Sample0_ADC4<11-00> & Fiber2_Sample0_ADC7<11-00> & Fiber0_Sample0_ADC6<11-08>
0x56:Data [65]    Fiber2_Sample0_ADC6<07-00> & Fiber3_Sample0_ADC7<11-00> & Fiber3_Sample0_ADC6<11-00>
-- Sample 1 for Fiber2/3 : sample1 read from actual circular buffer address - L1A latency + 1x25ns
0x57:Data [60]    Fiber2_Sample1_ADC1<11-00> & Fiber2_Sample1_ADC0<11-00> & Fiber3_Sample1_ADC1<11-04>
0x58:Data [61]    Fiber3_Sample1_ADC1<03-00> & Fiber3_Sample1_ADC0<11-00> & Fiber2_Sample1_ADC3<11-00> & Fiber0_Sample0_ADC2<11-08>
0x59:Data [62]    Fiber2_Sample1_ADC2<07-00> & Fiber3_Sample1_ADC3<11-00> & Fiber3_Sample1_ADC2<11-00>
0x5A:Data [63]    Fiber2_Sample1_ADC5<11-00> & Fiber2_Sample1_ADC4<11-00> & Fiber3_Sample1_ADC5<11-04>
0x5B:Data [64]    Fiber3_Sample1_ADC5<03-00> & Fiber3_Sample1_ADC4<11-00> & Fiber2_Sample1_ADC7<11-00> & Fiber0_Sample0_ADC6<11-08>
0x5C:Data [65]    Fiber2_Sample1_ADC6<07-00> & Fiber3_Sample1_ADC7<11-00> & Fiber3_Sample1_ADC6<11-00>
.
.
....Up to Fiber18/19 sample 9 if 10 samples to read
-- TRAILER---------------------------------------------------------------------------------------------------
0x263:Trailer[0]StatusElements        0x00000000
0x264:Trailer[1]DataElements        DataLength-9
0x265:Trailer[2]StatusBlockPosition
-------------------------------------------------------------------------------------------------------------
'''

class packet():

    def _init__(self):
        self.StartOfHeader
        self.HeaderSize
        self.FormatVersion
        self.SourceId
        self.RunNumber
        self.L1AId
        self.BCId
        self.L1ATriggerType
        self.DetectorEvtType
        self.ABBAHeader

        self.OpticalHeader

        self.Payload

        self.StatusElements
        self.DataElements
        self.StatusBlockPosition

class ABBA(packet):
    '''
    classdocs
    '''


    def __init__(self, fileName=None):
        '''
        Open file, read header, trailer, and
        '''

        self.file = open(fileName,"r")

        self.val0 = "aa1234aa"
        self.nSamples = 9

    def upackData(self,fragment):
        '''
            Unpack the headers, payload and trailler according to
            the information above
        '''

        pk = packet()

        pk.StartOfHeader   = fragment[0]
        pk.HeaderSize      = fragment[1]
        pk.FormatVersion   = fragment[2]
        pk.SourceId        = fragment[3]
        pk.RunNumber       = fragment[4]
        pk.L1AId           = fragment[5]
        pk.BCId            = fragment[6]
        pk.L1ATriggerType  = fragment[7]
        pk.DetectorEvtType = fragment[8]
        pk.ABBAHeader      = fragment[9]

        pk.OpticalHeader   = fragment[10]

        pk.StatusElements      = fragment[-3]
        pk.DataElements        = fragment[-2]
        pk.StatusBlockPosition = fragment[-1]


        #Format the payload accordingly
        #Include here the dictionary for mapping the fiber to SC
        #

        fiberSeq = [0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1]
        adcSeq =   [1,0,1,0,3,2,3,2,5,4,5,4,7,6,7,6]
        faseq = zip(fiberSeq,adcSeq)


        x = fragment[11:-3]
        lx = len(x)/6
        w = [(x[i]+x[i+1]+x[i+2]+x[i+3]+x[i+4]+x[i+5]) for i in range(lx)]

        self.nFiber = 11
        self.nADC = 8
        pl = {}
        k = 0
        for sample in range(self.nSamples) :
            for fa in faseq :
                idx = k*3
                wd =  w[idx:idx+3]
                print fa,sample,wd





    def readData(self,nevents = -1):

        self.data = []
        count = 0
        z = " "
        while z != "" :
            z = self.file.readline()
            zz = z.split()

            if len(zz)>0 :
                if zz[0] == self.val0 :
                    #create a function to unpack the data
                    dataHeader = zz[0:10]
                    opHeader = zz[10]
                    payload = zz[11:-3]
                    trailler=zz[-3:]
                    self.data.append([dataHeader,opHeader,payload,trailler])
                    count += 1

            if count%100 == 0 :
                print "Processed ", count, "events", len(zz)




x = ABBA("/data/LAr/ABBA/ABBAC_pc-lar-mon-01.cern.ch_1409233809.err")
x.readData()
x.upackData(fragment)

