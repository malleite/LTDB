#Quick test for printing from the root file

#use root_numpy later ..
import ROOT
import numpy as np
from root_numpy import tree2array

f = ROOT.TFile("test2.root")
t = f.Get("LTDB")

zz = tree2array(t)

fiber = zz["fiber"]
channel = zz["channel"]
samples = zz["samples"]
delay = zz["delay"]
rn = zz["runNumber"]


nev = samples.shape[0]

hh = ROOT.TH2F("hh","hh",100,0,2500,2000,0,2000)

myFiber   = 1
myChannel = 4
myRun = 274419
hh.Reset()

k  = 0
ni = 100
nf = 200

#ni = 0
#nf = 9600

hh.Reset()
j = 0
#get only samples where the maximum is above threshold
rr=samples[:,myFiber,myChannel,33]
ii = np.where(rr>950)
for i in ii[0] :
    #if rn[i] == myRun :
    if True :
        ww = samples[i][myFiber][myChannel]
        dd = delay[i]
        for k in range (60) :
            ti = (k*25) #+ (25-0.1*dd)
            ee = hh.Fill(ti,ww[k])
    if (j%100 == 0) : print "Done ",j, " Events"
    j += 1


hh.Draw()

