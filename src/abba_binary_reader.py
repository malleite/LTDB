'''
Created on Oct 1, 2014

@author: leite (leite@cern.ch)

Very simple ABBA reader and converter
'''


import binascii
import numpy as np
import progressbar
import os

from progressbar import AnimatedMarker, ProgressBar


import ROOT

from root_numpy import array2tree

ROOT = ROOT

class ABBA_converter(object):
    '''
        Simple converter for ABBA format to unpack the ADC data and convert it to something else
        Based on Jasmin unpacker and ATLAS event format specs
    '''

    def __init__(self, outFileName=None, condFileName=None,DEBUG=0):
        '''
            Open the file, call the definition of the header blocks and packet format
        '''
        #Read the conditions file
        self.getRunConditions(condFileName)

        #File to save the Tree
        self.outFileName = outFileName
        self.outFile = ROOT.TFile(self.outFileName,"recreate")

        self.DEBUG = DEBUG


################################################################################################

################################################################################################
    def prepareData(self,fname):

        fileInfo = os.stat(fname)
        self.file = open(fname,"r")
        pbar = ProgressBar(widgets=['Working: ', AnimatedMarker()])
        pbar = ProgressBar(maxval=fileInfo.st_size)

        self.data = []

        b = self.file.read(4)
        b =  binascii.hexlify(b[::-1])
        self.data.append(b)
        print " "
        print "----> INFO: Reading Data File: ",fname

        #TODO: read file first, then decode after
        kk = 0
        pbar.start()
        while b != "" :
            pbar.update(kk)
            b = self.file.read(4)
            b =  binascii.hexlify(b[::-1])
            self.data.append(b)
            kk = kk + 4
        print ""

################################################################################################

################################################################################################
    def eventDeclare(self):

        """
            Define the header names and structures. Note that the order in the list is important,
            as this is the way it will appear in the data
        """
        self.event = {}
        #self.eventKeyList = ["marker",        "fragmentSize",      "headerSize", "formatVersion", \
        #                     "sourceId",      "numStatusElements", "status",     "numSpecificHeaders", \
        #                     "time",          "timeNs",            "eventID",    "runType", \
        #                     "runNumber",     "lumiBlock",         "extEventID", "bcid", \
        #                     "L1TriggerType", "L1",                "L2",         "stream"]

        self.eventKeyList = ["marker",   "fragmentSize",      "headerSize",      "formatVersion", \
                             "sourceId", "numStatusElements", "status",          "numSpecificHeaders", \
                             "time",     "timeNs",            "eventID_LSB",     "eventID_MSB", \
                             "runType",  "runNumber",         "lumiBlock",       "extEventID", \
                             "bcid",     "L1TriggerType",     "CompressionType", "UncrompressedPayloadSize", \
                             "L1",       "L2",                "EF",              "stream" ]

        for key in self.eventKeyList :
            self.event[key] = None

        self.rob = {}
        self.robKeyList = ["marker",        "fragmentSize",      "headerSize", "formatVersion", \
                           "L1TriggerType", "numStatusElements", "t1",         "t2", \
                           "t3" ]

        for key in self.robKeyList :
            self.rob[key] = None

        self.rod = {}
        self.rodKeyList = ["marker",            "headerSize", "formatVersion",  "sourceId",  \
                           "runNumber",         "L1AId",      "bcid",           "L1ATriggerType", \
                           "detectorEventType", "payload",    "numStatusElements", "numDataElements", \
                           "statusBlockPosition" ]

        for key in self.rodKeyList :
            self.rod[key] = None


        self.event["marker"] = "aa1234aa"
        self.rob["marker"]   = "dd1234dd"
        self.rod["marker"]   = "ee1234ee"

        #these are hard-coded here. Check if the event header provides any of this information
        self.nSamples  = 60 #10
        self.nChannels = 8
        self.nFibers   = 20 #20
        self.nADCBits  = 12

        self.sourceId = "00410522"
        #self.sourceId = "abba0001"

        self.expectedPayloadSize = (self.nSamples * self.nChannels * self.nFibers * self.nADCBits ) /32 + 1
        #self.expectedPayloadSize = 3648
        self.tree = None

        #Now, create a simple array to store the fiber and channel in the ROOT tree
        self.channelArray = np.array([ np.arange(self.nChannels)]*self.nFibers)
        self.fiberArray   = np.array([ [i]*self.nChannels for i in range (self.nFibers) ] )

        #nSAmples * nChannels * nFibers = 1600
        #each samples is 12 bits -> 1600 * 12 = 19200
        #each word is 32 bits -> 19200 / 32 = 600 -> 600 words

################################################################################################

################################################################################################
    def unpack(self,payload):
        """
            Unpack the ABBA payload to a numpy array of dim [fibers,channels, samples]
        """
        fiberSeq = [0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1]
        adcSeq =   [1,0,1,0,3,2,3,2,5,4,5,4,7,6,7,6]
        faseq = zip(fiberSeq,adcSeq)

        #The first one is the optical header
        x = payload[1:]
        lx = len(x)
        ww = [(x[i]+x[i+1]+x[i+2]+x[i+3]+x[i+4]+x[i+5]) for i in range(0,lx,6)]

        adcData = np.zeros(self.nChannels * self.nFibers * self.nSamples,dtype=int)

        adcData.shape = (self.nFibers, self.nChannels, self.nSamples)


        for fiber in range (0,self.nFibers,2) :
            for sample in range(self.nSamples) :
                zx = sample + (fiber/2)*self.nSamples
                wx = ww[zx]
                k = 0
                for fa in faseq :
                    idx = k*3
                    wd =  wx[idx:idx+3]
                    fb = fa[0]+fiber
                    ch = fa[1]
                    val = int (wd,16)
                    adcData[fb,ch,sample] = val
                    k += 1

        return adcData

################################################################################################

    def convertData(self,adcData,doPrint = False):
        """
            Do something with this data .... (save as root file, bin numpy array etc ...
        """

        if doPrint :
            print adcData[:10]
            print adcData[10:]


################################################################################################

################################################################################################
    def fillTree(self,samples, evt, l1Counter, evtId_LSB, evtId_MSB):

        #root_numpy needs a record array
        self.runNumber = int (x.rod["runNumber"],16)
        #Need to protect in case ther eis no entry for this run Number
        dac = self.condDict[self.runNumber]["DAC"]
        delay = self.condDict[self.runNumber]["delay"]

        self.samples = samples
        aTuple = (self.runNumber, evt, l1Counter, evtId_LSB, evtId_MSB, dac, delay, self.fiberArray, self.channelArray, samples)
        fbShape = self.fiberArray.shape
        chShape = self.channelArray.shape
        spShape = samples.shape

        adc = np.array([aTuple], dtype=[("runNumber",int), ("event",int), ("l1Counter",int), ("evtId_LSB",int), ("evtId_MSB",int), ("dac",int), ("delay",int), ("fiber",int,fbShape), ("channel",int,chShape), ("samples",int,spShape)])
        if self.tree is None :
            self.tree = array2tree(adc, name="LTDB")

            print "----> INFO: Run:", self.runNumber, " DAC:", dac, " Delay:",delay

        else :
            #tree exists already, extend it
            #self.tree = array2tree(adc, name="LTDB", tree=self.tree)
            array2tree(adc, name="LTDB", tree=self.tree)
        #print "----> ", evt, self.tree.GetEntries()
################################################################################################

################################################################################################
    def processEvents(self,nevents=0,doPrint=False):
        """
            Read each event and extract the payload
        """


        evt = 0
        i = 0

        eventSize = 3650 #FIXME: get this number form the event record
        datalen = len(self.data)/eventSize

        if nevents <= 0 :
            nevents = datalen
            print "----> INFO: Will process ", datalen, "  events in datafile"

        else :
            print "----> INFO: Will process", nevents, " events in datafile"

        self.fiberData = []

        pbar = ProgressBar(widgets=['Working: ', AnimatedMarker()])

        pbar = ProgressBar(maxval=datalen)
        pbar.start()

        #while (i < datalen) and (evt < nevents) :
        while (evt < nevents) :
            pbar.update(evt)
            # print i,self.data[i]
            if self.data[i] == self.event["marker"] :
                if self.DEBUG > 0 :
                    print ""
                    print "Event ", evt
                    print 10*"= HEADER ="
                for k in self.eventKeyList :
                    self.event[k] = self.data[i]
                    if (self.DEBUG > 0) : print k, " : ", self.event[k]
                    i += 1
                evt += 1
                self.fiberData.append(None)

            if self.data[i] == self.rob["marker"] :
                if (self.DEBUG > 0) :
                    print ""
                    print 10*"= ROB ="
                for k in self.robKeyList :
                    self.rob[k] = self.data[i]
                    if (self.DEBUG > 0) : print k,  " : ", self.rob[k]
                    i += 1

            if self.data[i] == self.rod["marker"] :
                if (self.DEBUG > 0) :
                    print ""
                    print 10*"= ROD ="
                for k in self.rodKeyList :
                    if k == "payload" :
                        #Do not understand the 3 subtraction bellow
                        payloadSize = int("0x"+self.rob["fragmentSize"],16) - int("0x"+self.rob["headerSize"],16) - int("0x"+self.rod["headerSize"],16) - 3
                        #payloadSize = int(self.rob["fragmentSize"],16) - int(self.rob["headerSize"],16) - int(self.rod["headerSize"],16) - 7
                        #payloadSize = int(self.rob["fragmentSize"],16) + 2

                        if (self.DEBUG > 0) : print "payload (size) ", payloadSize
                        p = []
                        for j in range(payloadSize) :
                            p.append(self.data[i])
                            i += 1
                        self.rod["payload"] = p
                        opticalHeader = p[0]
                        if int("0x" + opticalHeader,16) != self.nSamples :
                            print "------> Inconsistent opticalHeader (\# of Samples) : Expected: ", self.nSamples, " <==> Got: ", opticalHeader, "(", hex(opticalHeader),")"

                        if (payloadSize == self.expectedPayloadSize) :
                            if self.rod["sourceId"] == self.sourceId :
                                adcData = self.unpack(self.rod["payload"])
                                self.fillTree(samples=adcData, evt=evt, \
                                              l1Counter =  int("0x" + self.rod["L1AId"], 16), \
                                              evtId_LSB =  int("0x" + self.event["eventID_LSB"], 16),  \
                                              evtId_MSB =  int("0x" + self.event["eventID_MSB"], 16) )
                                self.fiberData[-1] = adcData
                            else :
                                print "------> Inconsistent SourceID : Expected: ", self.sourceId, " <==> Got: ", self.rod["sourceId"]
                        else :
                            print "------> Inconsistent payload size with the conditions : Expect:", self.expectedPayloadSize, " <==> Got:", payloadSize
                    else :
                        self.rod[k] = self.data[i]
                        if (self.DEBUG > 0) : print k,  " : ", self.rod[k]
                        i += 1
            i += 1
        print ""
        print "----> INFO: Processed ", evt, " events  (", self.tree.GetEntries(), " valid )."

################################################################################################

################################################################################################
    def finalize(self):
        """
            Last things to be done
        """
        print "----> INFO: Saving tree to file: ", self.outFileName
        self.tree.Write()
        self.outFile.Close()

################################################################################################

################################################################################################
    def getRunConditions(self,condFileName=None):
        """
            Read a file that has the run conditions (dac, delay)
            format is runNumber \tab index \tab DAC \tab Delay \tab nEvents
            1st line is comment
            TODO: make something to ignore lines starting with #
        """
        print "----> INFO: Reading Run Conditions File: ", condFileName
        self.condDict = {}
        fileCond = open(condFileName)
        condList = fileCond.readlines()

        for kk in condList :
            if (kk[0][0] != "#") :
                ss = kk.split()
                if len(ss)>=5 :
                    runNumber = int(ss[0])
                    index = int(ss[1])
                    dac   = int(ss[2])
                    delay  = int(ss[3])
                    nEvents = int(ss[4])
                    self.condDict[runNumber] = {"index":index, "DAC":dac, "delay":delay, "nEvents":nEvents}



################################################################################################

if __name__ == "__main__":

    #Files are located in ~/eos/atlas/atlascerngroupdisk/det-larg/Demonstrator/Data/

    myFileList = []
    #myDir = "/afs/cern.ch/user/l/leite/eos/atlas/atlascerngroupdisk/det-larg/Demonstrator/Data/"
    myDir = "/Users/leite/ATLAS/LAr/ABBA/Data/ABBA/"
    #myFile = myDir + "data_test.00237668.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"
    #myFile = myDir + "data_test.00274444.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"

    #myFileList += [myDir + "data_test.00281708.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    #myFileList += [myDir + "data_test.00274238.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00295551.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    """
    myFileList += [myDir + "data_test.00274384.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274388.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274386.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274391.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274394.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274396.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274398.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274400.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274384.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274403.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274405.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274407.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274410.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274413.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274415.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274417.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274419.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274421.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274423.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274425.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274427.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274431.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274433.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274436.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    myFileList += [myDir + "data_test.00274438.calibration_.daq.RAW._lb0000._EB-EMF-05._0001.data"]
    """


    myOutFile = "test2.root"
    condFileName = "run_number.txt"
    x = ABBA_converter(outFileName=myOutFile,condFileName=condFileName,DEBUG=0)
    x.eventDeclare()

    for fname in myFileList :
        x.prepareData(fname)
        x.processEvents(-1, doPrint=True)

    x.finalize()









